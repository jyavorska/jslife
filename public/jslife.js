// Total size in cells of game board
var xSize = 450;
var ySize = 200;

// Size in pixes of each cell
var cellSizeX = 2;
var cellSizeY = 2;

// On randomize, the chance that each cell is pre-populated
var chancePopulated = 0.1;

// Game loop will slow itself down to match target, if needed
var targetFPS = 60;

// Maximum number of generations to run
var generationLimit = 2000;

// Initialize arrays for play area (active grid, plan which
// contains neighbor counts for next generation)
var grid = new Array(xSize).fill(false).map((x) => Array(ySize).fill(false));
var plan = new Array(xSize).fill(false).map((x) => Array(ySize).fill(false));

// Generation counter (for limiting)
let generationCounter = 0;

// Initialize PIXI and start game loop on load
let app;
let graphics;
let infoDisplay;

window.onload = function () {
  app = new PIXI.Application({
    width: xSize * cellSizeX,
    height: ySize * cellSizeY,
  });
  document.querySelector("#gameArea").appendChild(app.view);

  // Add graphics canvas to the stage
  graphics = new PIXI.Graphics();
  app.stage.addChild(graphics);

  // Add info display text
  infoDisplay = new PIXI.Text("Starting..", {
    fontFamily: "Arial",
    fontSize: 12,
    fill: 0xffffff,
    align: "left",
  });
  infoDisplay.x = 2; // Offset text from edge
  app.stage.addChild(infoDisplay);

  // Randomize initial board
  Randomize();

  // Set up ticker (game loop)
  app.ticker.minFPS = targetFPS;
  app.ticker.maxFPS = targetFPS;
  app.ticker.add(gameLoop);
};

function gameLoop(delta) {
  // Count generations and quit after this one if we get to the limit
  generationCounter++;
  if (generationCounter == generationLimit) {
    app.ticker.stop();
  }

  // Draw the current generation
  drawGraphics();

  // Create a plan for the next generation based on neighbor counts
  createPlan();

  // Execute the plan
  runPlan();
}

function drawGraphics() {
  // Draw the graphics
  graphics.clear();

  infoDisplay.text =
    "Generation: " + generationCounter + " of " + generationLimit;

  // Draw the background black
  graphics.beginFill(0x000000);
  graphics.drawRect(0, 0, xSize * cellSizeX, ySize * cellSizeY);
  graphics.endFill();

  // Fill in living cells
  for (x = 0; x < xSize; x++) {
    for (y = 0; y < ySize; y++) {
      if (grid[x][y]) {
        // Choose fill color based on number of neighbors
        if (plan[x][y] == 3) {
          // Fill brighter colors for more active cells
          graphics.beginFill(0x99aa99);
        } else if (plan[x][y] == 2) {
          graphics.beginFill(0x00aa00);
        } else {
          graphics.beginFill(0x222200);
        }
        graphics.drawRect(x * cellSizeX, y * cellSizeY, cellSizeX, cellSizeY);
        graphics.endFill();
      }
    }
  }
}

function Randomize() {
  // Sets every cell in the grid to a random on/off value, based on the
  // % chance of population
  for (x = 0; x < xSize; x++) {
    for (y = 0; y < ySize; y++) {
      if (Math.random() < chancePopulated) {
        grid[x][y] = true;
      } else {
        grid[x][y] = false;
      }
    }
  }
}

function createPlan() {
  // Create a new plan for the next generation via neighbor counting
  for (x = 0; x < xSize; x++) {
    for (y = 0; y < ySize; y++) {
      // The plan at this coordinate contains the count of neighbors, to
      // use later
      plan[x][y] = countNeighbors(x, y);
    }
  }
}

function runPlan() {
  // Apply Game of Life rules using neighbor counts
  for (x = 0; x < xSize; x++) {
    for (y = 0; y < ySize; y++) {
      if (grid[x][y] == true) {
        // If the current cell is alive..
        switch (true) {
          case plan[x][y] < 2:
            // Rule 1: Any live cell with fewer than two live neighbors
            // dies, as if by under population.
            grid[x][y] = false;
            break;
          case plan[x][y] > 3:
            // Rule 3: Any live cell with more than three live neighbors
            // dies, as if by overpopulation.
            grid[x][y] = false;
            break;
          default:
            // Rule 2: Any live cell with two or three live neighbors
            // lives on to the next generation.
            grid[x][y] = true;
            break;
        }
      } else {
        // If the current cell is dead..
        if (plan[x][y] == 3) {
          // Rule 4: Any dead cell with exactly three live neighbors
          // becomes a live cell, as if by reproduction.
          grid[x][y] = true;
        }
      }
    }
  }
}

function countNeighbors(x, y) {
  // Count neighbors to determine who lives and dies
  let neighbors = 0;

  // Helper values containing pointers to neighbor cells
  let upPos = x - 1;
  let downPos = x + 1;
  let leftPos = y - 1;
  let rightPos = y + 1;

  // Count neighbors in cardinal directions
  if (upPos >= 0) {
    if (grid[upPos][y]) {
      neighbors++;
    }
  }

  if (downPos < xSize) {
    if (grid[downPos][y]) {
      neighbors++;
    }
  }

  if (leftPos >= 0) {
    if (grid[x][leftPos]) {
      neighbors++;
    }
  }

  if (rightPos < ySize) {
    if (grid[x][rightPos]) {
      neighbors++;
    }
  }

  // Count diagonal neighbors
  if (upPos >= 0 && rightPos < ySize) {
    if (grid[upPos][rightPos]) {
      neighbors++;
    }
  }

  if (upPos >= 0 && leftPos >= 0) {
    if (grid[upPos][leftPos]) {
      neighbors++;
    }
  }

  if (downPos < xSize && rightPos < ySize) {
    if (grid[downPos][rightPos]) {
      neighbors++;
    }
  }

  if (downPos < xSize && leftPos >= 0) {
    if (grid[downPos][leftPos]) {
      neighbors++;
    }
  }

  return neighbors;
}
